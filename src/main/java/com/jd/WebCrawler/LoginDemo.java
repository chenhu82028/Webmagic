package com.jd.WebCrawler;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jsoup.Connection;
import org.jsoup.Connection.Method;
import org.jsoup.Connection.Response;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

public class LoginDemo {
	
	//保存登录后的cookies
	private static Map<String, String> map;
	
	private static StringBuffer str = new StringBuffer();
	
	public Map<String, String> getMap() {
		return map;
	};
	
    public static void main(String[] args) throws Exception {
    	
        LoginDemo loginDemo = new LoginDemo();
        
        //登录页面
        map = loginDemo.login("username", "password");// 输入用户名，和密码
        
        //进入列表网站    
        loginDemo.accessList("http://************",201);
        
        //将遍历的结果输出到文件中
        printTxt("D:/test.txt");
    }
    

    /**
     * 
     * @param searchEntry 传入封装好的搜索参数实体类
     * @return
     * @throws IOException
     */

	/**
     * 
     * @param filePath	存放文件的路径
     * @throws FileNotFoundException
     */
    private static void printTxt(String filePath) throws FileNotFoundException {
    	
		PrintWriter printWriter = new PrintWriter(filePath);
		printWriter.println(str.toString());
		printWriter.flush();
		printWriter.close();
	}

	/**
     * 
     * @param string	请求的url
     * @param cookies	cookies信息
     * @throws IOException 
     */
    private void accessList(String url,int page) throws IOException {
    	
    	for (int i = 1; i <= page; i++) {
    		
    		accessList(url + "?page=" + i);
    		
		}
	}
    
    /**
     * 
     * @param url 请求的URL
     * @param cookies	登陆后获取的cookies
     * @throws IOException	
     */
    private void accessList(String url) throws IOException {
    	
    	//获取连接
		Connection connect = Jsoup.connect(url);
		
		Response login = connect.ignoreContentType(true).method(Method.GET).cookies(map).execute();
		
		//将响应结果转为DOM树
		Document doc = Jsoup.parse(login.body());
		
		//获取表单主体的信息
		List<Element> select = doc.select("td");
		
		for (int i = 0;i<select.size();i++) {
			
			if(i % 6 == 0 ){
				str.append("\r\n" + select.get(i).text());
			}else{
				str.append(", " + select.get(i).text() );
			}
			
		}
		
	}


	/**
     * @param userName
     *            用户名
     * @param pwd
     *            密码
     * **/
    public Map<String,String> login(String userName, String pwd) throws Exception {
        // 第一次请求
        Connection con = Jsoup.connect("http://*********");// 获取连接
        
//        con.header("User-Agent","Mozilla/5.0 (Windows NT 6.1; WOW64; rv:29.0) Gecko/20100101 Firefox/29.0");// 配置模拟浏览器
        
        Response rs = con.execute();// 获取响应
        
        Document d1 = Jsoup.parse(rs.body());// 转换为Dom树
        
        List<Element> et = d1.select("#form1");// 获取form表单，可以通过查看页面源码代码得知
        
        // 获取，cooking和表单属性，下面map存放post时的数据
        Map<String, String> datas = new HashMap<>();
        
        for (Element e : et.get(0).getAllElements()) {
            if (e.attr("name").equals("userNumber")) {
                e.attr("value", userName);// 设置用户名
            }
            if (e.attr("name").equals("userPwd")) {
                e.attr("value", pwd); // 设置用户密码
            }
            
            if (e.attr("name").length() > 0) {// 排除空值表单属性
                datas.put(e.attr("name"), e.attr("value"));
            }
        }
        
        /**
         * 第二次请求，post表单数据，以及cookie信息
         * 
         * **/
        Connection con2 = Jsoup.connect("http://***********");
        
//        con2.header("User-Agent","Mozilla/5.0 (Windows NT 6.1; WOW64; rv:29.0) Gecko/20100101 Firefox/29.0");
        
        // 设置cookie和post上面的map数据
        Response login = con2.ignoreContentType(true).method(Method.POST).data(datas).cookies(rs.cookies()).execute();
        
        // 登陆成功后的cookie信息，可以保存到本地，以后登陆时，只需一次登陆即可
        map = login.cookies();
        
        return map;
    }

}